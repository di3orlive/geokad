jQuery(function($){

    $(".phoneRequired").mask("8 (999) 999-99-99");

    $("#ajaxform1").submit(function(){ // перехватываем все при событии отправки
        var form = $(this); // запишем форму, чтобы потом не было проблем с this
        var error = false; // предварительно ошибок нет
        form.find('input, textarea').each( function(){ // пробежим по каждому полю в форме
            if ($(this).val() == '') { // если находим пустое
                alert('Заполните поле "'+$(this).attr('placeholder')+'"!'); // говорим заполняй!
                error = true; // ошибка
            }
        });
        if (!error) { // если ошибки нет
            var data = form.serialize(); // подготавливаем данные
            $.ajax({ // инициализируем ajax запрос
                type: 'POST', // отправляем в POST формате, можно GET
                url: 'email_s1.php', // путь до обработчика, у нас он лежит в той же папке
                dataType: 'json', // ответ ждем в json формате
                data: data, // данные для отправки
                beforeSend: function(data) { // событие до отправки
                    form.find('input[type="submit"]').attr('disabled', 'disabled'); // например, отключим кнопку, чтобы не жали по 100 раз
                },
                success: function(data){ // событие после удачного обращения к серверу и получения ответа
                    if (data['error']) { // если обработчик вернул ошибку
                        alert(data['error']); // покажем её текст
                    } else{ // если все прошло ок

                        // alert('Письмо отвравлено! Чекайте почту! =)'); // пишем что все ок
                        setTimeout(function(){
                            $('#ajaxform1').fadeOut(100);
                            $('#thanks1').fadeIn(400);
                            console.log('block');
                        }, 0000);
                        setTimeout(function(){
                            $('#thanks1').fadeOut(100);
                            $('#ajaxform1').fadeIn(400);
                            console.log('block2');
                        }, 5000);
                        var tel1 = document.getElementById('tel1');
                        tel1.value = '';
                        var tel2 = document.getElementById('tel2');
                        tel2.value = '';
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) { // в случае неудачного завершения запроса к серверу
                    alert(xhr.status); // покажем ответ сервера
                    alert(thrownError); // и текст ошибки
                },
                complete: function(data) { // событие после любого исхода
                    form.find('input[type="submit"]').prop('disabled', false); // в любом случае включим кнопку обратно
                }

            });
        }
        return false; // вырубаем стандартную отправку формы
    });
    // #ajaxform

    $("#ajaxform2").submit(function(){ // перехватываем все при событии отправки
        var form = $(this); // запишем форму, чтобы потом не было проблем с this
        var error = false; // предварительно ошибок нет
        form.find('input, textarea').each( function(){ // пробежим по каждому полю в форме
            if ($(this).val() == '') { // если находим пустое
                alert('Заполните поле "'+$(this).attr('placeholder')+'"!'); // говорим заполняй!
                error = true; // ошибка
            }
        });
        if (!error) { // если ошибки нет
            var data = form.serialize(); // подготавливаем данные
            $.ajax({ // инициализируем ajax запрос
                type: 'POST', // отправляем в POST формате, можно GET
                url: 'email_s2.php', // путь до обработчика, у нас он лежит в той же папке
                dataType: 'json', // ответ ждем в json формате
                data: data, // данные для отправки
                beforeSend: function(data) { // событие до отправки
                    form.find('input[type="submit"]').attr('disabled', 'disabled'); // например, отключим кнопку, чтобы не жали по 100 раз
                },
                success: function(data){ // событие после удачного обращения к серверу и получения ответа
                    if (data['error']) { // если обработчик вернул ошибку
                        alert(data['error']); // покажем её текст
                    } else{ // если все прошло ок

                        // alert('Письмо отвравлено! Чекайте почту! =)'); // пишем что все ок
                        setTimeout(function(){
                            $('#ajaxform2').fadeOut(100);
                            $('#thanks2').fadeIn(400);
                            console.log('block3');
                        }, 0000);
                        setTimeout(function(){
                            $('#thanks2').fadeOut(100);
                            $('#ajaxform2').fadeIn(400);
                            console.log('block4');
                        }, 5000);
                        var tel3 = document.getElementById('tel3');
                        tel3.value = '';
                        var tel4 = document.getElementById('tel4');
                        tel4.value = '';
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) { // в случае неудачного завершения запроса к серверу
                    alert(xhr.status); // покажем ответ сервера
                    alert(thrownError); // и текст ошибки
                },
                complete: function(data) { // событие после любого исхода
                    form.find('input[type="submit"]').prop('disabled', false); // в любом случае включим кнопку обратно
                }

            });
        }
        return false; // вырубаем стандартную отправку формы
    });
    // #ajaxform





$("#ajaxform3").submit(function(){ // перехватываем все при событии отправки
        var form = $(this); // запишем форму, чтобы потом не было проблем с this
        var error = false; // предварительно ошибок нет
        form.find('input, textarea').each( function(){ // пробежим по каждому полю в форме
            if ($(this).val() == '') { // если находим пустое
                alert('Заполните поле "'+$(this).attr('placeholder')+'"!'); // говорим заполняй!
                error = true; // ошибка
            }
        });
        if (!error) { // если ошибки нет
            var data = form.serialize(); // подготавливаем данные
            $.ajax({ // инициализируем ajax запрос
                type: 'POST', // отправляем в POST формате, можно GET
                url: 'email_s3.php', // путь до обработчика, у нас он лежит в той же папке
                dataType: 'json', // ответ ждем в json формате
                data: data, // данные для отправки
                beforeSend: function(data) { // событие до отправки
                    form.find('input[type="submit"]').attr('disabled', 'disabled'); // например, отключим кнопку, чтобы не жали по 100 раз
                },
                success: function(data){ // событие после удачного обращения к серверу и получения ответа
                    if (data['error']) { // если обработчик вернул ошибку
                        alert(data['error']); // покажем её текст
                    } else{ // если все прошло ок

                        // alert('Письмо отвравлено! Чекайте почту! =)'); // пишем что все ок
                        setTimeout(function(){
                            $('#ajaxform3').fadeOut(100);
                            $('#thanks3').fadeIn(400);
                            console.log('block5');
                        }, 0000);
                        setTimeout(function(){
                            $('#thanks3').fadeOut(100);
                            $('#ajaxform3').fadeIn(400);
                            console.log('block6');
                        }, 5000);
                        var tel5 = document.getElementById('tel5');
                        tel5.value = '';
                        var tel6 = document.getElementById('tel6');
                        tel6.value = '';
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) { // в случае неудачного завершения запроса к серверу
                    alert(xhr.status); // покажем ответ сервера
                    alert(thrownError); // и текст ошибки
                },
                complete: function(data) { // событие после любого исхода
                    form.find('input[type="submit"]').prop('disabled', false); // в любом случае включим кнопку обратно
                }

            });
        }
        return false; // вырубаем стандартную отправку формы
    });
    // #ajaxform

});



jQuery(function($){

    $( '#cbp-ntaccordion' ).cbpNTAccordion();
    $( '#cbp-ntaccordion2' ).cbpNTAccordion();
    $( '#cbp-ntaccordion3' ).cbpNTAccordion();

    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".our_services_img1_li_img1"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            div.removeClass('greenBorder1');// скрываем его
            $('.cbp-ntopen1').css({"display":"none"});
        }
    });
    $(".our_services_img1_li_img1").click(function(e){
        if($(this).hasClass('greenBorder1')){
            $(this).removeClass('greenBorder1');
        }else{
            $(this).addClass('greenBorder1');
            $('.cbp-ntopen1').css({"display":"block"});

            $(".our_services_img1_li_img2").removeClass('greenBorder2');
            $(".our_services_img1_li_img3").removeClass('greenBorder3');
            $(".our_services_img2_li_img1").removeClass('greenBorder2-1');
            $(".our_services_img2_li_img2").removeClass('greenBorder2-2');
            $(".our_services_img2_li_img3").removeClass('greenBorder2-3');
            $(".our_services_img3_li_img1").removeClass('greenBorder3-1');
            $(".our_services_img3_li_img2").removeClass('greenBorder3-2');
            $(".our_services_img3_li_img3").removeClass('greenBorder3-3');
        }
    });



    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".our_services_img1_li_img2"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            div.removeClass('greenBorder2');// скрываем его
            $('.cbp-ntopen2').css({"display":"none"});
        }
    });
    $(".our_services_img1_li_img2").click(function(){
        if($(this).hasClass('greenBorder2')){
            $(this).removeClass('greenBorder2');
        }else{
            $(this).addClass('greenBorder2');
            $(".our_services_img1_li_img1").removeClass('greenBorder1');
            $('.cbp-ntopen2').css({"display":"block"});

            $(".our_services_img1_li_img3").removeClass('greenBorder3');
            $(".our_services_img2_li_img1").removeClass('greenBorder2-1');
            $(".our_services_img2_li_img2").removeClass('greenBorder2-2');
            $(".our_services_img2_li_img3").removeClass('greenBorder2-3');
            $(".our_services_img3_li_img1").removeClass('greenBorder3-1');
            $(".our_services_img3_li_img2").removeClass('greenBorder3-2');
            $(".our_services_img3_li_img3").removeClass('greenBorder3-3');
        }
    });




    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".our_services_img1_li_img3"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            div.removeClass('greenBorder3');// скрываем его
            $('.cbp-ntopen3').css({"display":"none"});
        }
    });
    $(".our_services_img1_li_img3").click(function(){
        if($(this).hasClass('greenBorder3')){
            $(this).removeClass('greenBorder3');
        }else{
            $(this).addClass('greenBorder3');
            $(".our_services_img1_li_img1").removeClass('greenBorder1');
            $(".our_services_img1_li_img2").removeClass('greenBorder2');
            $('.cbp-ntopen3').css({"display":"block"});

            $(".our_services_img2_li_img1").removeClass('greenBorder2-1');
            $(".our_services_img2_li_img2").removeClass('greenBorder2-2');
            $(".our_services_img2_li_img3").removeClass('greenBorder2-3');
            $(".our_services_img3_li_img1").removeClass('greenBorder3-1');
            $(".our_services_img3_li_img2").removeClass('greenBorder3-2');
            $(".our_services_img3_li_img3").removeClass('greenBorder3-3');
        }
    });
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".our_services_img2_li_img1"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            div.removeClass('greenBorder2-1');// скрываем его
            $('.cbp-ntopen1').css({"display":"none"});
        }
    });
    $(".our_services_img2_li_img1").click(function(){
        if($(this).hasClass('greenBorder2-1')){
            $(this).removeClass('greenBorder2-1');
        }else{
            $(this).addClass('greenBorder2-1');
            $(".our_services_img1_li_img1").removeClass('greenBorder1');
            $(".our_services_img1_li_img2").removeClass('greenBorder2');
            $(".our_services_img1_li_img3").removeClass('greenBorder3');
            $('.cbp-ntopen1').css({"display":"block"});

            $(".our_services_img2_li_img2").removeClass('greenBorder2-2');
            $(".our_services_img2_li_img3").removeClass('greenBorder2-3');
            $(".our_services_img3_li_img1").removeClass('greenBorder3-1');
            $(".our_services_img3_li_img2").removeClass('greenBorder3-2');
            $(".our_services_img3_li_img3").removeClass('greenBorder3-3');
        }
    });



    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".our_services_img2_li_img2"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            div.removeClass('greenBorder2-2');// скрываем его
            $('.cbp-ntopen2').css({"display":"none"});
        }
    });
    $(".our_services_img2_li_img2").click(function(){
        if($(this).hasClass('greenBorder2-2')){
            $(this).removeClass('greenBorder2-2');
        }else{
            $(this).addClass('greenBorder2-2');
            $(".our_services_img1_li_img1").removeClass('greenBorder1');
            $(".our_services_img1_li_img2").removeClass('greenBorder2');
            $(".our_services_img1_li_img3").removeClass('greenBorder3');
            $(".our_services_img2_li_img1").removeClass('greenBorder2-1');
            $('.cbp-ntopen2').css({"display":"block"});

            $(".our_services_img2_li_img3").removeClass('greenBorder2-3');
            $(".our_services_img3_li_img1").removeClass('greenBorder3-1');
            $(".our_services_img3_li_img2").removeClass('greenBorder3-2');
            $(".our_services_img3_li_img3").removeClass('greenBorder3-3');
        }
    });




    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".our_services_img2_li_img3"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            div.removeClass('greenBorder2-3');// скрываем его
            $('.cbp-ntopen3').css({"display":"none"});
        }
    });
    $(".our_services_img2_li_img3").click(function(){
        if($(this).hasClass('greenBorder2-3')){
            $(this).removeClass('greenBorder2-3');
        }else{
            $(this).addClass('greenBorder2-3');
            $(".our_services_img1_li_img1").removeClass('greenBorder1');
            $(".our_services_img1_li_img2").removeClass('greenBorder2');
            $(".our_services_img1_li_img3").removeClass('greenBorder3');
            $(".our_services_img2_li_img1").removeClass('greenBorder2-1');
            $(".our_services_img2_li_img2").removeClass('greenBorder2-2');
            $('.cbp-ntopen3').css({"display":"block"});

            $(".our_services_img3_li_img1").removeClass('greenBorder3-1');
            $(".our_services_img3_li_img2").removeClass('greenBorder3-2');
            $(".our_services_img3_li_img3").removeClass('greenBorder3-3');
        }
    });
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".our_services_img3_li_img1"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            div.removeClass('greenBorder3-1');// скрываем его
            $('.cbp-ntopen1').css({"display":"none"});
        }
    });
    $(".our_services_img3_li_img1").click(function(){
        if($(this).hasClass('greenBorder3-1')){
            $(this).removeClass('greenBorder3-1');
        }else{
            $(this).addClass('greenBorder3-1');
            $(".our_services_img1_li_img1").removeClass('greenBorder1');
            $(".our_services_img1_li_img2").removeClass('greenBorder2');
            $(".our_services_img1_li_img3").removeClass('greenBorder3');
            $(".our_services_img2_li_img1").removeClass('greenBorder2-1');
            $(".our_services_img2_li_img2").removeClass('greenBorder2-2');
            $(".our_services_img2_li_img3").removeClass('greenBorder2-3');
            $('.cbp-ntopen1').css({"display":"block"});

            $(".our_services_img3_li_img2").removeClass('greenBorder3-2');
            $(".our_services_img3_li_img3").removeClass('greenBorder3-3');
        }
    });




    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".our_services_img3_li_img2"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            div.removeClass('greenBorder3-2');// скрываем его
            $('.cbp-ntopen2').css({"display":"none"});
        }
    });
    $(".our_services_img3_li_img2").click(function(){
        if($(this).hasClass('greenBorder3-2')){
            $(this).removeClass('greenBorder3-2');
        }else{
            $(this).addClass('greenBorder3-2');
            $(".our_services_img1_li_img1").removeClass('greenBorder1');
            $(".our_services_img1_li_img2").removeClass('greenBorder2');
            $(".our_services_img1_li_img3").removeClass('greenBorder3');
            $(".our_services_img2_li_img1").removeClass('greenBorder2-1');
            $(".our_services_img2_li_img2").removeClass('greenBorder2-2');
            $(".our_services_img2_li_img3").removeClass('greenBorder2-3');
            $(".our_services_img3_li_img1").removeClass('greenBorder3-1');
            $('.cbp-ntopen2').css({"display":"block"});

            $(".our_services_img3_li_img3").removeClass('greenBorder3-3');
        }
    });





    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".our_services_img3_li_img3"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            div.removeClass('greenBorder3-3');// скрываем его
            $('.cbp-ntopen3').css({"display":"none"});
        }
    });
    $(".our_services_img3_li_img3").click(function(){
        if($(this).hasClass('greenBorder3-3')){
            $(this).removeClass('greenBorder3-3');
        }else{
            $(this).addClass('greenBorder3-3');
            $(".our_services_img1_li_img1").removeClass('greenBorder1');
            $(".our_services_img1_li_img2").removeClass('greenBorder2');
            $(".our_services_img1_li_img3").removeClass('greenBorder3');
            $(".our_services_img2_li_img1").removeClass('greenBorder2-1');
            $(".our_services_img2_li_img2").removeClass('greenBorder2-2');
            $(".our_services_img2_li_img3").removeClass('greenBorder2-3');
            $(".our_services_img3_li_img1").removeClass('greenBorder3-1');
            $(".our_services_img3_li_img2").removeClass('greenBorder3-2');
            $('.cbp-ntopen3').css({"display":"block"});

        }
    });



    $('.post').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated flipInX',
        offset: 10
    });

    $('.post2').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated bounceInUp', // Class to add to the elements when they are visible
        offset: 100
    });
    $('.post3').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated fadeInDown', // Class to add to the elements when they are visible
        offset: 100
    });



    $(".rslides").responsiveSlides({
        timeout: 5000
    });



    $('.bxslider').bxSlider({
        nextText: "",
        prevText: "",
        speed: 1000,
        auto: true,
        pause: 10000,
        autoHover: true
    });



    $('.callme_button').jbcallme({
        no_name: true,
        no_tel: true,
        no_submit: true,
        title: "Обратный звонок",
        success: "Форма успешно заполнена и отправлена! <br/> Мы обязательно свяжемся с Вами. <br/>  Спасибо!",
        fields: { // добавление полей
            name:{
                placeholder: "*Ваше имя",
                required: true
            },
            company: {
                required: false,
                placeholder: "Ваша фамилия",
                type: "text"
            },
            tel:{
                placeholder: "*Номер телефона",
                required: true
            },
            email: {
                required: false,
                placeholder: "Ваш e-mail",
                type: "email"
            },
            send: {
                label: "*обязательное поле",
                type: "submit",
                value: "Заказать звонок"
            }
        }
    });



    $("a.grouped_elements").fancybox({
        'transitionIn'	:	'elastic',
        'transitionOut'	:	'elastic',
        'speedIn'		:	600,
        'speedOut'		:	200,
        'overlayShow'	:	true
    });
    $(".grouped_elements2, .grouped_elements3").fancybox({
        'transitionIn'	:	'elastic',
        'transitionOut'	:	'elastic',
        'speedIn'		:	600,
        'speedOut'		:	200,
        'overlayShow'	:	true,
        'autoPlay'      :   true,
        'playSpeed'     :   100
    });




    var $window           = $(window),
        win_height_padded = $window.height() * 1.1,
        isTouch           = Modernizr.touch;

    if (isTouch) { $('.revealOnScroll').addClass('animated'); }

    $window.on('scroll', revealOnScroll);

    function revealOnScroll() {
        var scrolled = $window.scrollTop(),
            win_height_padded = $window.height() * 1.1;

        // Showed...
        $(".revealOnScroll:not(.animated)").each(function () {
            var $this     = $(this),
                offsetTop = $this.offset().top;

            if (scrolled + win_height_padded > offsetTop) {
                if ($this.data('timeout')) {
                    window.setTimeout(function(){
                        $this.addClass('animated ' + $this.data('animation'));
                    }, parseInt($this.data('timeout'),10));
                } else {
                    $this.addClass('animated ' + $this.data('animation'));
                }
            }
        });
        // Hidden...
        $(".revealOnScroll.animated").each(function (index) {
            var $this     = $(this),
                offsetTop = $this.offset().top;
            if (scrolled + win_height_padded < offsetTop) {
                $(this).removeClass('animated fadeInUp flipInX lightSpeedIn')
            }
        });
    }

    revealOnScroll();

});